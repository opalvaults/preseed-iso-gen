#!/bin/sh

DIR=$(pwd)
TMPDIR=$(mktemp -d)
trap 'rm -rf $TMPDIR' EXIT

init_check () {

    help="Usage: sudo preseedisogen.sh [DEBIAN .ISO] [PRESEED.CFG] [DEBIAN VERSION]"

    if [ "$(id -u)" != 0 ]; then
        echo "Must be executed with sudo, or as root."
        echo "$help"
        exit 1

    elif [ $# != 3 ]; then
        echo "Must have 3 arguments."
        echo "$help"
        exit 1

    elif [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
        echo "$help"
        exit 0

    fi

}

package_check () {

    echo "Checking for correct packages.."

    packages="xorriso isolinux libarchive-tools genisoimage syslinux-utils"

    for pkg in $packages; do
        if apt-cache policy "$pkg" | grep -i "Installed: (none)"; then
            echo "$pkg not found, installing $pkg.."
            apt-get -y -qq install "$pkg"
        fi
    done

}

iso_extract () {

    echo "Extracting ISO.."

    case $1 in
        *.iso)
            cp -a "$1" "$TMPDIR"
            bsdtar -C "$TMPDIR" -xf "$TMPDIR"/*.iso
            ;;

        *)
            echo "Error: Missing .iso argument, or wrong extension."
            exit 1
            ;;
    esac

}

preseed_initrd () {

    echo "Unpacking, baking preseed, and repacking.."

    case $2 in
        *.cfg)
            chmod +w -R "$TMPDIR"/install.*/
            gunzip "$TMPDIR"/install.*/initrd.gz
            cp -a -f "$2" preseed.cfg
            echo preseed.cfg | cpio -H newc -o -A -F "$TMPDIR"/install.*/initrd > /dev/null 2>&1
            gzip "$TMPDIR"/install.*/initrd
            chmod -w -R "$TMPDIR"/install.*/
            ;;

        *)
            printf "Error: Missing preseed.cfg argument, or wrong extension
            (must be a preseed file w/ .cfg extension).\n" >&2
            exit 1
            ;;
    esac

}

regen_md5 () {

    echo "Regenerating md5sum.txt.."
    cd "$TMPDIR" || exit
    chmod +w md5sum.txt
    # Find execs md5sum on current dir, awks the hash, sorts the hashes,
    # and md5sums the entire directory.
    find . -type f -exec md5sum {} + | awk '{print $1}' | sort | md5sum > md5sum.txt
    chmod -w md5sum.txt

}

#Takes contents of /tmp/isofiles, and recreates bootable .iso.
iso_recreate () {

    mbr_template=/usr/lib/ISOLINUX/isohdpfx.bin

    echo "Creating new preseeded ISO.."
    cd "$TMPDIR" || exit

    xorriso -as mkisofs \
        -r -V debian-"$3"-amd64 \
        -o preseeded-debian-"$3".iso \
        -J -J -joliet-long -cache-inodes \
        -isohybrid-mbr "$mbr_template" \
        -b isolinux/isolinux.bin \
        -c isolinux/boot.cat \
        -boot-load-size 4 -boot-info-table -no-emul-boot \
        -eltorito-alt-boot \
        -e boot/grub/efi.img \
        -no-emul-boot -isohybrid-gpt-basdat -isohybrid-apm-hfsplus \
        "$TMPDIR" > /dev/null 2>&1

}

clean_up () {

    mv "$TMPDIR"/preseeded-debian-"$3".iso "$DIR"
    chmod -R 0644 "$DIR"/preseeded-debian-"$3".iso
    chown -R "$(logname)":"$(logname)" "$DIR"/preseeded-debian-"$3".iso
    rm -f "$DIR"/preseed.cfg
    echo "Success! The .iso has been created in $DIR."

}

main () {

    init_check "$@"
    package_check
    iso_extract "$@"
    preseed_initrd "$@"
    regen_md5
    iso_recreate "$@"
    clean_up "$@"

}

main "$@"
